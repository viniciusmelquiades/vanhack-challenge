## Vanhack PET

This project was made to demonstrate an use of map and saving data i a form.

The basis is, you report a lost pet and it will appear in the map so others can help you find it.

There is no API. All is in memory.

Because of time constraints, everywhere I needed a position, I used the device's.

There are also no tests. Shame on me :(

The error handling is poor

To run the project:

- install react native
- run npm install (this will install dependencies)
- npm run tsc (compile typescript)
- npm run rn (this will run the bundler)
- react-native run-android (this will run the app)

I recommend a real device with location enabled.
