import { configure } from 'mobx';
import { Provider } from 'mobx-react';
import React, { Component } from 'react';
import { Router } from './components/Routes';
import { AppStore } from './stores/AppStore';

configure({ enforceActions: true });

export default class App extends Component {
	private readonly appStore = new AppStore();

	render() {
		return (
			<Provider appStore={this.appStore}>
				<Router />
			</Provider>
		);
	}
}
