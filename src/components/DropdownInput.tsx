import { bind } from 'bind-decorator';
import { runInAction } from 'mobx';
import React from 'react';
import { Picker, PickerProps } from 'react-native';
import { IInputBaseProps, InputBase } from './InputBase';

export interface IInputProps<T = any, TValue = any>
	extends IInputBaseProps,
		PickerProps {
	selectedValue: TValue;
	onValueChange?: (item: any, index: number) => void;
	model?: T;
	prop?: keyof T;
	source: Array<{ label: string; value: TValue }>;
}

export class DropdownInput extends React.Component<IInputProps> {
	@bind
	private onValueChange(value: any, index: number) {
		const { onValueChange, model, prop } = this.props;

		if (onValueChange) {
			onValueChange(value, index);
		}

		if (model && prop) {
			runInAction(() => {
				model[prop] = value;
			});
		}
	}

	render() {
		const { label, viewStyle, selectedValue, source, ...props } = this.props;

		return (
			<InputBase label={label} viewStyle={viewStyle}>
				<Picker
					selectedValue={selectedValue}
					onValueChange={this.onValueChange}
					{...props}
				>
					{source.map((item, index) => (
						<Picker.Item key={index} label={item.label} value={item.value} />
					))}
				</Picker>
			</InputBase>
		);
	}
}
