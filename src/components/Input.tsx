import React from 'react';
import {
	TextInputProps,
	Text,
	View,
	TextInput,
	TextInputComponent
} from 'react-native';
import { bind } from 'bind-decorator';
import { runInAction } from 'mobx';
import { InputBase, IInputBaseProps } from './InputBase';

export interface IInputProps<T = any> extends IInputBaseProps, TextInputProps {
	text: string;
	onTextChanged?: (text: string) => void;
	model?: T;
	prop?: keyof T;
}

export class Input extends React.Component<IInputProps> {
	@bind
	private onTextChanged(text: string) {
		const { onTextChanged, model, prop } = this.props;

		if (onTextChanged) {
			onTextChanged(text);
		}

		if (model && prop) {
			runInAction(() => {
				model[prop] = text;
			});
		}
	}

	render() {
		const { label, viewStyle, text, ...props } = this.props;

		return (
			<InputBase label={label} viewStyle={viewStyle}>
				<TextInput {...props} value={text} onChangeText={this.onTextChanged} />
			</InputBase>
		);
	}
}
