import React from 'react';
import {
	TextInputProps,
	Text,
	View,
	TextInput,
	TextInputComponent
} from 'react-native';
import { bind } from 'bind-decorator';
import { runInAction } from 'mobx';

export interface IInputBaseProps {
	label: string;
	viewStyle?: any;
}

export class InputBase extends React.Component<IInputBaseProps> {
	render() {
		const { label, viewStyle, children } = this.props;

		return (
			<View style={viewStyle}>
				<Text>{label}</Text>
				{children}
			</View>
		);
	}
}
