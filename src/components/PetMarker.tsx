import React from 'react';
import { Pet } from '../models/Pet';
import { Marker } from 'react-native-maps';
import { PetType } from '../models/PetType';
import { computed } from 'mobx';
import { NavigationScreenProps } from 'react-navigation';
import { observer } from 'mobx-react';
import bind from 'bind-decorator';

export interface IPetMarkerProps {
	pet: Pet;
	onOpenDetails: (pet: Pet) => void;
}

@observer
export class PetMarker extends React.Component<IPetMarkerProps> {
	@computed
	private get title() {
		let type;

		switch (this.props.pet.type) {
			case PetType.DOG:
				type = ' - Dog';
				break;
			case PetType.CAT:
				type = ' - Cat';
				break;
			case PetType.BIRD:
				type = ' - Bird';
				break;
			default:
				type = '';
				break;
		}

		return `${this.props.pet.name}${type} - Click to open details`;
	}

	render() {
		const pet = this.props.pet;
		return (
			<Marker
				title={this.title}
				coordinate={pet.lastPosition}
				onCalloutPress={this.onCalloutPress}
			/>
		);
	}

	@bind
	private onCalloutPress() {
		this.props.onOpenDetails(this.props.pet);
	}
}
