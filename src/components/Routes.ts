import {
	createStackNavigator,
	createBottomTabNavigator,
	createMaterialTopTabNavigator
} from 'react-navigation';
import { MapScreen } from '../screens/MapScreen';
import { FormScreen } from '../screens/FormScreen';
import { DetailsScreen } from '../screens/DetailsScreen';

export const Router = createStackNavigator(
	{
		Map: { screen: MapScreen },
		Form: { screen: FormScreen },
		Details: { screen: DetailsScreen }
	},
	{
		initialRouteName: 'Map',
		navigationOptions: {
			title: 'Vanhack Pet'
		}
	}
);
