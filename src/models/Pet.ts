import { observable, action } from 'mobx';
import { LatLng } from 'react-native-maps';
import { PetType } from './PetType';

export class Pet {
	constructor(data?: {}) {
		if (data) Object.assign(this, data);
	}

	@observable id: number;
	@observable name: string;
	@observable type: PetType;
	@observable description: string;
	@observable contactInfo: string;
	@observable.shallow lastPosition: LatLng;

	isValid() {
		return (
			this.name &&
			this.type &&
			this.description &&
			this.contactInfo &&
			this.lastPosition
		);
	}

	@action
	setId(id: number) {
		this.id = id;
	}
}
