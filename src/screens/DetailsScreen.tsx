import React from 'react';
import { NavigationScreenProps } from 'react-navigation';
import { View, Text, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { Pet } from '../models/Pet';
import { PetType } from '../models/PetType';
import { computed } from 'mobx';

const styles = StyleSheet.create({
	container: { padding: 15 } as ViewStyle,
	label: { paddingTop: 10 } as TextStyle,
	field: { fontWeight: 'bold' } as TextStyle
});

export class DetailsScreen extends React.Component<NavigationScreenProps> {
	private readonly pet = this.props.navigation.getParam('pet') as Pet;

	@computed
	get typeString() {
		switch (this.pet.type) {
			case PetType.DOG:
				return 'Dog';
			case PetType.CAT:
				return 'Cat';
			case PetType.BIRD:
				return 'Bird';
			default:
				return 'Other animal. See description';
				break;
		}
	}

	render() {
		const { name, description, contactInfo } = this.pet;

		return (
			<View style={styles.container}>
				<Text>Name:</Text>
				<Text style={styles.field}>{name}</Text>

				<Text style={styles.label}>Animal kind:</Text>
				<Text style={styles.field}>{this.typeString}</Text>

				<Text style={styles.label}>Descrption:</Text>
				<Text style={styles.field}>{description}</Text>

				<Text style={styles.label}>Contact info:</Text>
				<Text style={styles.field}>{contactInfo}</Text>
			</View>
		);
	}
}
