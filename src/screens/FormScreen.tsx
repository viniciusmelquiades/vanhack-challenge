import { observable, action, runInAction } from 'mobx';
import { observer, inject } from 'mobx-react';
import React from 'react';
import {
	View,
	Button,
	StyleSheet,
	ViewStyle,
	KeyboardAvoidingView,
	ScrollView,
	GeolocationError,
	Text,
	TextStyle
} from 'react-native';
import { LatLng } from 'react-native-maps';
import { DropdownInput } from '../components/DropdownInput';
import { Input } from '../components/Input';
import { bind } from 'bind-decorator';
import { PetType } from '../models/PetType';
import { Pet } from '../models/Pet';
import { addPet } from '../services/PetsFakeService';
import { NavigationScreenProps } from 'react-navigation';
import { AppStore } from '../stores/AppStore';

const styles = StyleSheet.create({
	container: { padding: 15 } as ViewStyle,
	inputContainer: { paddingTop: 10 } as ViewStyle,
	lastLocationDescription: {
		fontWeight: 'bold'
	} as TextStyle,
	errorMessage: { marginBottom: 20 } as TextStyle
});

export interface IFormScreenProps extends NavigationScreenProps {
	appStore?: AppStore;
}

@inject('appStore')
@observer
export class FormScreen extends React.Component<IFormScreenProps> {
	@observable private readonly model = new Pet({ type: PetType.DOG });

	private readonly typeOptions = [
		{
			label: 'Dog',
			value: PetType.DOG
		},
		{
			label: 'Cat',
			value: PetType.CAT
		},
		{
			label: 'Bird',
			value: PetType.BIRD
		},
		{
			label: 'Other (please specify in description below)',
			value: PetType.OTHER
		}
	];
	private readonly appStore = this.props.appStore!;
	private readonly mapStore = this.appStore.mapStore;

	@observable private error: { message: string; canTryAgain: boolean };

	componentDidMount() {
		this.getLocation();
	}

	getLocation() {
		navigator.geolocation.getCurrentPosition(
			position => this.setPosition(position.coords),
			error => this.setPositionError(error)
		);
	}

	render() {
		if (this.error) {
			return (
				<View style={styles.container}>
					<Text style={styles.errorMessage}>{this.error.message}</Text>
					{this.error.canTryAgain ? (
						<Button title="Try again" onPress={this.clearError} />
					) : (
						false
					)}
				</View>
			);
		}

		return (
			<View style={styles.container}>
				<ScrollView>
					<Text style={styles.lastLocationDescription}>
						The place your pet was last seen will be set on your current
						location.
					</Text>
					<Input
						label="Name"
						text={this.model.name}
						model={this.model}
						prop="name"
						viewStyle={styles.inputContainer}
					/>
					<DropdownInput
						label="What kind of animal is it?"
						selectedValue={this.model.type}
						model={this.model}
						prop="type"
						source={this.typeOptions}
						viewStyle={styles.inputContainer}
					/>
					<Input
						label="Describe your pet. How does it look like?"
						text={this.model.description}
						model={this.model}
						prop="description"
						multiline
						numberOfLines={3}
						viewStyle={styles.inputContainer}
					/>
					<Input
						label="How can we concact you when we find your friend?"
						text={this.model.contactInfo}
						model={this.model}
						prop="contactInfo"
						multiline
						viewStyle={styles.inputContainer}
					/>
					<Button title="Save" onPress={this.save} />
				</ScrollView>
			</View>
		);
	}

	@action
	private setPosition(position: LatLng) {
		this.model.lastPosition = position;
		this.error = null;
	}

	@action
	private setPositionError(error: GeolocationError) {
		this.error = {
			message: `The report is created at your current location, but we failed to get it. Please go back and try again.\n\n${
				error.message
			}`,
			canTryAgain: false
		};
	}

	@action.bound
	private clearError() {
		this.error = null;
	}

	@bind
	private async save() {
		if (!this.model.isValid()) {
			console.log('pet is invalid', JSON.stringify(this.model));
			runInAction(
				() =>
					(this.error = {
						message: 'All fields are required. Please fill them.',
						canTryAgain: true
					})
			);
			return;
		}

		this.model.setId(await addPet(this.model));

		this.mapStore.addPet(this.model);
		this.props.navigation.goBack();
	}
}
