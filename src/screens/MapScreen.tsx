import React from 'react';
import {
	Text,
	Button,
	StyleSheet,
	TextStyle,
	View,
	GeolocationError
} from 'react-native';
import { default as MapView, Marker, LatLng } from 'react-native-maps';
import {
	NavigationScreenProps,
	NavigationScreenOptions,
	NavigationActions
} from 'react-navigation';
import { observer, inject } from 'mobx-react';
import { observable, action, runInAction } from 'mobx';
import { Pet } from '../models/Pet';
import { PetMarker } from '../components/PetMarker';
import { getAllPets } from '../services/PetsFakeService';
import { AppStore } from '../stores/AppStore';
import { bind } from 'bind-decorator';

const styles = StyleSheet.create({
	map: {
		...StyleSheet.absoluteFillObject
	}
});

export interface IMapScreenProps extends NavigationScreenProps {
	appStore?: AppStore;
}

@inject('appStore')
@observer
export class MapScreen extends React.Component<IMapScreenProps> {
	static navigationOptions = ({
		navigation
	}: NavigationScreenProps): NavigationScreenOptions => ({
		headerRight: (
			<Button
				onPress={() => navigation.navigate('Form')}
				title="Report missing pet"
			/>
		)
	});

	private readonly appStore = this.props.appStore!;
	private readonly mapStore = this.appStore.mapStore;

	render() {
		if (this.appStore.error) {
			return (
				<View>
					<Text>There was an error getting the location:</Text>
					<Text>{this.appStore.error.message}</Text>
				</View>
			);
		}

		if (!this.appStore.center) {
			return <Text>Getting position</Text>;
		}

		return (
			<MapView
				initialRegion={{
					latitude: this.appStore.center.latitude,
					longitude: this.appStore.center.longitude,
					latitudeDelta: 0.015,
					longitudeDelta: 0.0121
				}}
				style={styles.map}
			>
				{this.getMarkers()}
			</MapView>
		);
	}

	private getMarkers() {
		return this.mapStore.pets.map(pet => (
			<PetMarker onOpenDetails={this.onOpenDetails} pet={pet} key={pet.id} />
		));
	}

	@bind
	private onOpenDetails(pet: Pet) {
		this.props.navigation.navigate('Details', { pet });
	}
}
