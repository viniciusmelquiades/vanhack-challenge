import { Pet } from '../models/Pet';

/**
 * This service will return fake data. All data will stay in memory and will be lost if the app is killed.
 */

const data = [
	{
		id: 1,
		name: 'Snow',
		description: "all white. Doesn't like to be picked up",
		type: 'CAT',
		contactInfo: 'Just call me on Facebook',
		lastPosition: {
			longitude: -46.7023444,
			latitude: -23.5828853
		}
	}
];

/** Creates a delay between 0 and 1 second, to simulate network */
const randomDelay = () =>
	new Promise(resolve => setTimeout(resolve, Math.random() * 10));

export const getAllPets = async () => {
	await randomDelay();
	return data.map(data => new Pet(data));
};

export const addPet = async (pet: Pet) => {
	await randomDelay();
	const newId = data[0].id + 1;
	const raw = {
		id: newId,
		name: pet.name,
		description: pet.description,
		type: pet.type,
		contactInfo: pet.contactInfo,
		lastPosition: { ...pet.lastPosition }
	};

	console.log('added pet', raw);
	data.push(raw);
	return newId;
};
