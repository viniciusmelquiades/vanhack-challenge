import { MapStore } from './MapStore';
import { GeolocationError } from 'react-native';
import { observable, action } from 'mobx';
import { LatLng } from 'react-native-maps';

export class AppStore {
	mapStore = new MapStore();

	@observable.shallow center: LatLng = null;
	@observable.shallow error: GeolocationError = null;

	constructor() {
		this.getLocation();
	}

	getLocation() {
		navigator.geolocation.getCurrentPosition(
			position => this.setCenter(position.coords),
			error => this.setLocationError(error)
		);
	}

	@action
	private setCenter(coords: LatLng) {
		console.log('setCenter', coords);
		this.center = coords;
		this.error = null;
	}

	@action
	private setLocationError(err: GeolocationError) {
		this.center = null;
		this.error = err;
	}
}
