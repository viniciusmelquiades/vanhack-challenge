import { observable, runInAction, action } from 'mobx';
import { Pet } from '../models/Pet';
import { getAllPets } from '../services/PetsFakeService';

export class MapStore {
	@observable.shallow pets: Pet[] = [];

	constructor() {
		this.loadPets();
	}

	async loadPets() {
		const pets = await getAllPets();
		runInAction(() => (this.pets = pets));
	}

	@action
	addPet(pet: Pet) {
		this.pets = [...this.pets, pet];
	}
}
